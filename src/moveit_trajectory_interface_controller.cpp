/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Ioan A. Sucan
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Ioan A. Sucan nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Lucas Joseph */

#include <ros/ros.h>
#include <moveit_trajectory_interface_controller/action_based_controller_handle.h>
#include <sensor_msgs/JointState.h>
#include <pluginlib/class_list_macros.hpp>
#include <map>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

namespace moveit_trajectory_interface_controller
{

//   class TrajectoryAction
// {
//   private:
//    actionlib::SimpleActionClient<moveit_trajectory_interface::TrajectoryAction> ac;
// public:
//   TrajectoryAction() : ac("/trajectory_action", true)
//   {
//     ROS_INFO("Waiting for action server to start.");
//     ac.waitForServer();
//     ROS_INFO("Action server started, sending goal.");
//   }    

//   void sendGoal(moveit_msgs::RobotTrajectory path)
//   {
//     moveit_trajectory_interface::TrajectoryGoal goal;
//     goal.path = path;
//     goal.max_velocity_scaling_factor = 0.1;
//     goal.max_acceleration_scaling_factor = 0.1;
//     goal.start = true;
//     goal.trajectory_time_increment = 0.001;
//     const ros::Duration test;
//     ac.sendGoal(goal);
//   }

//   void waitForResult()
//   {
//     ac.waitForResult();
//   }
// };

class ExampleControllerHandle 
  : public ActionBasedControllerHandle
{
private:
  // TrajectoryAction trajectory_action;
public:
  ExampleControllerHandle(const std::string& name, const std::string& action_ns) 
    : ActionBasedControllerHandle(name, action_ns)
  {

  }

  bool sendTrajectory(const moveit_msgs::RobotTrajectory& t) override
  {
    // ROS_ERROR_STREAM("controller_action_client_.getState()" << controller_action_client_->getState());
    if (!controller_action_client_)
      return false;

    if (done_)
      ROS_DEBUG_STREAM("sending trajectory to " << name_);
    else
      ROS_DEBUG_STREAM("sending continuation for the currently executed trajectory to " << name_);

    moveit_trajectory_interface::TrajectoryGoal goal;
    goal.path = t;
    goal.max_velocity_scaling_factor = 0.1;
    goal.max_acceleration_scaling_factor = 0.1;

    controller_action_client_->sendGoal(goal,
                                      std::bind(&ExampleControllerHandle::controllerDoneCallback, this,
                                                std::placeholders::_1, std::placeholders::_2),
                                      std::bind(&ExampleControllerHandle::controllerActiveCallback, this),
                                      std::bind(&ExampleControllerHandle::controllerFeedbackCallback,
                                                this, std::placeholders::_1));
    done_ = false;
    last_exec_ = moveit_controller_manager::ExecutionStatus::RUNNING;

    return true;
  }

  void controllerActiveCallback()
  {
    ROS_DEBUG_STREAM("Started execution");
  }

  void controllerDoneCallback(
    const actionlib::SimpleClientGoalState& state, const moveit_trajectory_interface::TrajectoryResultConstPtr& result)
  {
    // Output custom error message for FollowJointTrajectoryResult if necessary
    if (!result)
      ROS_DEBUG_STREAM("Controller '" << name_ << "' done, no result returned");
    else 
      ROS_DEBUG_STREAM("Controller '" << name_ << "' done");
    finishControllerExecution(state);
  }

  void controllerFeedbackCallback(
    const moveit_trajectory_interface::TrajectoryFeedbackConstPtr& /* feedback */)
  {
  }
  // bool cancelExecution() override
  // {
  //   // do whatever is needed to cancel execution
  //   return true;
  // }

  // bool waitForExecution(const ros::Duration& /*timeout*/) override
  // {
  //   // trajectory_action.waitForResult();
  //   ROS_WARN_STREAM("Trajectory Done");
  //   // wait for the current execution to finish
  //   return true;
  // }

  moveit_controller_manager::ExecutionStatus getLastExecutionStatus() override
  {
    return moveit_controller_manager::ExecutionStatus(moveit_controller_manager::ExecutionStatus::SUCCEEDED);
  }
};

class MoveItControllerManagerExample : public moveit_controller_manager::MoveItControllerManager
{
public:
  MoveItControllerManagerExample()
  {
    ROS_DEBUG_STREAM("Using my moveit_trajectory_interface_controller"); 
  }

  ~MoveItControllerManagerExample() override = default;

  moveit_controller_manager::MoveItControllerHandlePtr getControllerHandle(const std::string& name) override
  {
    ROS_DEBUG_STREAM("getControllerHandle name " << name) ;
    ROS_ERROR_STREAM("getControllerHandle name " << node_handle_.getNamespace()) ;
    return moveit_controller_manager::MoveItControllerHandlePtr(new ExampleControllerHandle("/"+node_handle_.getNamespace()+"/"+name,""));
  }

  /*
   * Get the list of controller names.
   */
  void getControllersList(std::vector<std::string>& names) override
  {
    names.resize(1);
    names[0] = "trajectory_action";
  }

  /*
   * This plugin assumes that all controllers are already active -- and if they are not, well, it has no way to deal
   * with it anyways!
   */
  void getActiveControllers(std::vector<std::string>& names) override
  {
    getControllersList(names);
  }

  /*
   * Controller must be loaded to be active, see comment above about active controllers...
   */
  virtual void getLoadedControllers(std::vector<std::string>& names)
  {
    getControllersList(names);
  }

  /*
   * Get the list of joints that a controller can control.
   */
  void getControllerJoints(const std::string& name, std::vector<std::string>& joints) override
  {
    joints.clear();
    if (name == "trajectory_action")
    { 
      std::vector<std::string> joint_names;
      ROS_DEBUG_STREAM( " node_handle_.getNamespace() " << node_handle_.getNamespace()) ;
      node_handle_.getParam("move_group/"+name+"/joints", joint_names);
      for (auto i: joint_names)
      {
        joints.push_back(i);
      } 
    }
  }

  /*
   * Controllers are all active and default.
   */
  moveit_controller_manager::MoveItControllerManager::ControllerState
  getControllerState(const std::string& /*name*/) override
  {
    moveit_controller_manager::MoveItControllerManager::ControllerState state;
    state.active_ = true;
    state.default_ = true;
    return state;
  }

  /* Cannot switch our controllers */
  bool switchControllers(const std::vector<std::string>& /*activate*/,
                         const std::vector<std::string>& /*deactivate*/) override
  {
    return false;
  }
protected:
  ros::NodeHandle node_handle_;
  std::map<std::string, moveit_controller_manager::MoveItControllerHandlePtr> controllers_;
};

}  // end namespace moveit_trajectory_interface_controller

PLUGINLIB_EXPORT_CLASS(moveit_trajectory_interface_controller::MoveItControllerManagerExample,
                       moveit_controller_manager::MoveItControllerManager);
